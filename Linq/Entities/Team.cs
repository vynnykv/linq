﻿using System;
using System.Collections.Generic;

namespace Linq.Entities
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<User> Users { get; set; }

        public override string ToString()
        {
            return $"id: {Id}\n" +
                   $"name: {Name}\n" +
                   $"createdAt: {CreatedAt}";
        }
    }
}