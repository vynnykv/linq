﻿namespace Linq.Entities
{
    public enum TaskState
    {
        Created,
        Cancelled,
        Done,
        InProgress
    }
}