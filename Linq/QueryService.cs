﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Linq.Entities;
using Linq.ViewModels;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Task = Linq.Entities.Task;

namespace Linq
{
    public class QueryService
    {
        private readonly IConfigurationRoot _config;
        private readonly HttpClient _httpClient;
        private readonly string baseUrl;

        public QueryService(
            IConfigurationRoot config,
            HttpClient httpClient)
        {
            _config = config;
            _httpClient = httpClient;
            baseUrl = _config.GetSection("Url").Value;
        }
        
        public async Task<Dictionary<Project, int>> GetTaskQuantityByUser(int userId)
        {
            var tasks = await GetEntities<Task>("tasks");
            var projects = await GetEntities<Project>("projects");
            var result = projects
                .Where(pr => pr.AuthorId == userId)
                .GroupJoin(
                    tasks,
                    pr => pr.Id,
                    t => t.ProjectId,
                    (project, task) => new
                    {
                        Project = project,
                        numsOfTasks = task.Count()
                    })
                .ToDictionary(r => r.Project, r => r.numsOfTasks);
            return result;
        }

        public async Task<IEnumerable<Task>> GetTaskLimitedSymbols(int userId, int symbolsQuantity)
        {
            var tasks = await GetEntities<Task>("tasks");
            return tasks
                .Where(t => t.PerformerId == userId && t.Name.Length < symbolsQuantity);
        }

        public async Task<IEnumerable<(int id, string name)>> GetFinishedTasks(int userId, int year)
        {
            var tasks = await GetEntities<Task>("tasks");
            return tasks
                .Where(t => t.FinishedAt?.Year == year && t.PerformerId == userId)
                .Select(r => (r.Id, r.Name));
        }

        public async Task<IEnumerable<(int id, string name, IEnumerable<User> users)>> GetOlderUsers(int age)
        {
            var teams = await GetEntities<Team>("teams");
            var users = await GetEntities<User>("users");
            var result = teams.GroupJoin(
                    users,
                    t => t.Id,
                    u => u.TeamId,
                    (team, user) => new
                    {
                        Team = team,
                        Users = user
                            .Where(u => (DateTime.Now.Year - u.BirthDay.Year) > age)
                            .OrderByDescending(u=>u.RegisteredAt)
                            .Select(u=>u)
                    })
                .Select(r => (r.Team.Id, r.Team.Name, r.Users));
            return result;
        }
        
        public async Task<IEnumerable<(User user, IEnumerable<Task> tasks)>> GetSortedUsersWithTasks()
        {
            var users = await GetEntities<User>("users");
            var tasks = await GetEntities<Task>("tasks");
            var result = users.OrderBy(u=>u.FirstName)
                .GroupJoin(
                    tasks,
                    u => u.Id,
                    t => t.PerformerId,
                    (u, t) => new
                    {
                        user = u, task = t.OrderByDescending(x => x.Name.Length)
                    })
                .Select(r => (r.user, r.task.AsEnumerable()));
            return result;
        }

        public async Task<UserInfoVm> GetUserInfo(int userId)
        {
            var users = await GetEntities<User>("users");
            var projects = await GetEntities<Project>("projects");
            var tasks = await GetEntities<Task>("tasks");
            var result = users.GroupJoin(projects, user => user.Id, project => project.AuthorId,
                    (user, proj) => new User()
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        RegisteredAt = user.RegisteredAt,
                        BirthDay = user.BirthDay,
                        Projects = proj.GroupJoin(tasks, p => p.Id, task => task.ProjectId,
                                (project, task) => new Project()
                                {
                                    Id = project.Id,
                                    Name = project.Name,
                                    Description = project.Description,
                                    Deadline = project.Deadline,
                                    AuthorId = project.AuthorId,
                                    TeamId = project.TeamId,
                                    CreatedAt = project.CreatedAt,
                                    Tasks = task
                                        .ToList()
                                })
                            .ToList(),
                        Tasks = tasks
                            .Where(t => t.PerformerId == user.Id)
                            .ToList()
                    }
                ).Select(user => new UserInfoVm()
                {
                    User = user,
                    LastProject = user.Projects.FirstOrDefault(p => p.CreatedAt == user.Projects.Max(p => p.CreatedAt)),
                    TasksQuantityLastProject =
                        user.Projects.FirstOrDefault(p => p.CreatedAt == user.Projects.Max(p => p.CreatedAt))?.Tasks
                            .Count() ?? 0,
                    GeneralQuantityCancelledTasks = user.Tasks.Count(t=>t.State == TaskState.Cancelled),
                    GeneralQuantityUnfinishedTasks = user.Tasks.Count(t=>t.FinishedAt == null),
                    LongestTaskByDuration = user.Tasks
                        .Where(t=>t.FinishedAt is not null)
                        .FirstOrDefault(t=>t.FinishedAt.Value - t.CreatedAt == user.Tasks.Where(t=>t.FinishedAt is not null).Max(t=>t.FinishedAt.Value - t.CreatedAt))
                })
                .FirstOrDefault(u=>u.User.Id == userId);
            return result;
        }

        private async Task<List<T>> GetEntities<T>(string route) where T : class
        {
            var response = await _httpClient.GetAsync(baseUrl + $"{route}");
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<T>>(json);
        }
        private async Task<T> GetEntityById<T>(string route, int entityId) where T : class
        {
            var response = await _httpClient.GetAsync(baseUrl + $"{route}/{entityId}");
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}